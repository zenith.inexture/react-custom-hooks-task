import { useState } from "react";
import useFetchData from "./components/useFetchData";
import useWindowSize from "./components/useWindowSize";
import usePosition from "./components/usePosition";
import useValidation from "./components/useValidation";
import useStorage from "./components/useStorage";
import useOperation from "./components/useOperation";
import useCopy from "./components/useCopy";
import "./App.css";

function App() {
  //calling fetch data hook
  const [data] = useFetchData("https://jsonplaceholder.typicode.com/users", []);

  //calling windowSize hook
  const [size] = useWindowSize();

  //calling position hook
  const { positionlat, positionlng } = usePosition();

  //validation state and calling it's hooks
  const [email, setEmail] = useState("");
  const [err, validateEmail] = useValidation(email);
  //function that run on onclick submit button
  const onSubmitFun = (e) => {
    e.preventDefault();
    validateEmail(email);
  };

  //state for storage
  const [name, setName] = useState("");
  const [target, setTarget] = useState("localstorage");
  //function calling
  const storevalue = useStorage(name, target);
  const onStorageFun = (e) => {
    e.preventDefault();
    storevalue(name, target);
  };

  //origin array defined
  const originalarr = [3, 5, 8, 4, 2, 0, 6, 8];
  //state for array operation and call function
  const [ope, setOpe] = useState("sort");
  const [array, run] = useOperation(ope);
  //on submit function for array operation
  const onSubmitope = (e) => {
    e.preventDefault();
    run(ope);
  };

  //copy text state
  const [copytext, setCopytext] = useState("");

  return (
    <div className="App">
      <>
        {/* fetchdata hook  */}
        <h2>useFetchData hook</h2>
        {data &&
          data.map((item) => {
            return <p key={item.id}>{item.name}</p>;
          })}

        {/* windowsize hook  */}
        <h2>useWindowSize hook</h2>
        <div>
          Width is: {size.width}px AnD Height is : {size.height}px
        </div>
        <div>Device: {size.device}</div>

        {/* position hook  */}
        <h2>usePosition hook</h2>
        <p>latitude: {positionlat}</p>
        <p>longitude: {positionlng}</p>

        {/* Email validation hook  */}
        <h2>useValidation hook</h2>
        <form>
          <label>
            Email:
            <input
              type="email"
              name="email"
              onChange={(e) => setEmail(e.target.value)}
              value={email}
            ></input>
          </label>
          {err && <p>{err}</p>}
          <button type="submit" onClick={onSubmitFun}>
            Submit
          </button>
        </form>

        {/* storage hook  */}
        <h2>useStorage hook</h2>
        <form>
          <label>
            Name:
            <input
              type="text"
              name="name"
              onChange={(e) => setName(e.target.value)}
              value={name}
            ></input>
          </label>
          <select onChange={(e) => setTarget(e.target.value)}>
            <option value="localstorage">Localstorage</option>
            <option value="sessionstorage">SessionStorage</option>
            <option value="both">Both</option>
          </select>
          <button type="submit" onClick={onStorageFun}>
            Store
          </button>
        </form>

        {/* Array operation hook  */}
        <h2>useOperation Hook</h2>
        {originalarr.map((i, index) => {
          return <span key={index}>{i} </span>;
        })}
        <form>
          <select onChange={(e) => setOpe(e.target.value)}>
            <option value="sort">Sort</option>
            <option value="push">Push 0 at last position</option>
            <option value="pop">Pop last element</option>
            <option value="slice">Slice half array</option>
          </select>
          <button type="submit" onClick={onSubmitope}>
            Submit
          </button>
        </form>
        {array.map((i, index) => {
          return <span key={index}>{i} </span>;
        })}

        {/* copy to clipboard hook  */}
        <h2>useCopy Hook</h2>
        <form>
          <input
            type="text"
            value={copytext}
            onChange={(e) => setCopytext(e.target.value)}
          ></input>
          {/* call clipboard custom hook - it's return the html button*/}
          {useCopy(copytext)}
          <br></br>
          <br></br>
          <textarea></textarea>
        </form>
      </>
    </div>
  );
}

export default App;
