import { useState } from "react";

const useOperation = () => {
  const arr = [3, 5, 8, 4, 2, 0, 6, 8];
  const [array, setAnswer] = useState(arr);
  const run = (operation) => {
    if (operation === "sort") {
      setAnswer(arr.sort());
    } else if (operation === "push") {
      arr.push(0);
      setAnswer(arr);
    } else if (operation === "pop") {
      arr.pop();
      setAnswer(arr);
    } else if (operation === "slice") {
      const newarr = arr.slice(0, arr.length / 2);
      setAnswer(newarr);
    }
  };
  return [array, run];
};
export default useOperation;
