import { useState, useEffect } from "react";

const usePosition = () => {
  const [positionlat, setPositionLat] = useState(null);
  const [positionlng, setPositionLng] = useState(null);

  useEffect(() => {
    navigator.geolocation.getCurrentPosition((position) => {
      setPositionLat(position.coords.latitude);
      setPositionLng(position.coords.longitude);
    });
  });

  return { positionlat, positionlng };
};
export default usePosition;
