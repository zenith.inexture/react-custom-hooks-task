import { useState } from "react";
import { CopyToClipboard } from "react-copy-to-clipboard";

const useCopy = (textt) => {
  const [result, setResult] = useState();
  return (
    <>
      <CopyToClipboard text={textt} onCopy={() => setResult(true)}>
        <button type="submit">Copy</button>
      </CopyToClipboard>
      {result ? <p>Copied!!</p> : null}
    </>
  );
};
export default useCopy;
