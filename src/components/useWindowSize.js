import { useState, useEffect } from "react";

const useWindowSize = () => {
  const [size, setSize] = useState({
    width: 0,
    height: 0,
    device: "",
  });
  useEffect(() => {
    function windowsize() {
      if (window.innerWidth >= 768) {
        setSize({
          width: window.innerWidth,
          height: window.innerHeight,
          device: "Laptop",
        });
      } else if (window.innerWidth >= 480) {
        setSize({
          width: window.innerWidth,
          height: window.innerHeight,
          device: "Tablet",
        });
      } else {
        setSize({
          width: window.innerWidth,
          height: window.innerHeight,
          device: "Mobile",
        });
      }
    }
    window.addEventListener("resize", windowsize); //call when screen size changed

    windowsize(); //call first time

    //remove event if we can't remove then it's going into loop
    return () => window.removeEventListener("resize", windowsize);
  }, []);
  return [size];
};
export default useWindowSize;
