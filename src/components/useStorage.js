const useStorage = (name, target) => {
  const storevalue = () => {
    if (target === "localstorage") {
      localStorage.setItem("name", JSON.stringify(name));
    } else if (target === "sessionstorage") {
      sessionStorage.setItem("name", JSON.stringify(name));
    } else {
      localStorage.setItem("name", JSON.stringify(name));
      sessionStorage.setItem("name", JSON.stringify(name));
    }
  };
  return storevalue;
};
export default useStorage;
