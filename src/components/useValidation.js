import { useState } from "react";

const useValidation = () => {
  const [error, setError] = useState();

  const validateEmail = (value) => {
    let err = "";
    if (!value) {
      err = "Email is required!!";
    } else if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i.test(value)) {
      err = "Email address is invalid";
    }
    setError(err);
  };
  return [error, validateEmail];
};

export default useValidation;
